print("""The axe game
by Запо / Zapo
""")

input("Press enter to continue.") 

tut = input("""Would you like to see a tutorial?
1. Yes
2. No """)

if tut == "1":
    print("""This is a game for 2 players.
You have to guess 10 numbers between 1 and 100.
You have 20 vertical lines, each has 5 numbers
and a horizontal line which is where the number is
""")

print("""A man dropped his axes in a hole and you have to guess where it is.
Dont worry you have another person to help you
""")

name_1 = "P1"
name_2 = "P2"

p_names = input("""Do you want to set player names
1. Yes
2. No """)

if p_names == "1":
    name_1 = input("""Player 1 name: """)
    name_2 = input("""Player 2 name: """)
    print(f"""Player 1:{name_1}
Player 2:{name_2}""")

axe_1_cor_1 = False
axe_1_cor_2 = False

axe_1_num = "39"

axe_1_loc = """1.
5   |
10  |
15  |
20  |
25  |
30  |
35  |
40  |-
45  |
50  |
55  |
60  |
65  |
70  |
75  |
80  |
85  | 
90  |
95  |
100 |
"""
print(axe_1_loc)

axe_1_guess_1 = input(f"{name_1}, Where is the axe?")
axe_1_guess_2 = input(f"{name_2}, Where is  the axe?")

if axe_1_guess_1 == (axe_1_num):
    axe_1_cor_1 = True

if axe_1_guess_2 == (axe_1_num):
    axe_1_cor_2 = True

axe_2_cor_1 = False
axe_2_cor_2 = False

axe_2_num = "66"

axe_2_loc = """2.
5   |  
10  |
15  |
20  |
25  |
30  |
35  |
40  |
45  |
50  |
55  |
60  |
65  |
70  |-
75  |
80  |
85  | 
90  |
95  |
100 |
"""
print(axe_2_loc)

axe_2_guess_1 = input(f"{name_1}, Where is the axe?")
if axe_2_guess_1 == (axe_2_num):
    axe_2_cor_1 = True
    
axe_2_guess_2 = input(f"{name_2}, Where is the axe?")
if axe_2_guess_2 == (axe_2_num):
    axe_2_cor_2 = True

axe_3_cor_1 = False
axe_3_cor_2 = False

axe_3_num = "41"

axe_3_loc = """3.
5   |  
10  |
15  |
20  |
25  |
30  |
35  |
40  |
45  |-
50  |
55  |
60  |
65  |
70  |
75  |
80  |
85  | 
90  |
95  |
100 |
"""
print(axe_3_loc)

axe_3_guess_1 = input(f"{name_1}, Where is the axe?")
if axe_3_guess_1 == (axe_3_num):
    axe_3_cor_1 = True
    
axe_3_guess_2 = input(f"{name_2}, Where is the axe?")
if axe_3_guess_2 == (axe_3_num):
    axe_3_cor_2 = True

axe_4_cor_1 = False
axe_4_cor_2 = False

axe_4_num = "2" 

axe_4_loc = """4.
5   |-
10  |
15  |
20  |
25  |
30  |
35  |
40  |
45  |
50  |
55  |
60  |
65  |
70  |
75  |
80  |
85  | 
90  |
95  |
100 |
"""
print(axe_4_loc)

axe_4_guess_1 = input(f"{name_1}, Where is the axe?")
if axe_4_guess_1 == (axe_4_num):
    axe_4_cor_1 = True
    
axe_4_guess_2 = input(f"{name_2}, Where is the axe?")
if axe_4_guess_2 == (axe_4_num):
    axe_4_cor_2 = True

axe_5_cor_1 = False
axe_5_cor_2 = False

axe_5_num = "27"

axe_5_loc = """5.
5   |-
10  |
15  |
20  |
25  |
30  |-
35  |
40  |
45  |
50  |
55  |
60  |
65  |
70  |
75  |
80  |
85  | 
90  |
95  |
100 |
"""
print(axe_5_cor_1)

axe_5_guess_1 = input(f"{name_1}, Where is the axe?")
if axe_5_guess_1 == (axe_5_num):
    axe_5_cor_1 = True
    
axe_5_guess_2 = input(f"{name_2}, Where is the axe?")
if axe_5_guess_2 == (axe_5_num):
    axe_5_cor_2 = True

axe_6_cor_1 = False
axe_6_cor_2 = False

axe_6_num = "60"

axe_6_loc = """6.
5   |-
10  |
15  |
20  |
25  |
30  |
35  |
40  |
45  |
50  |
55  |
60  |-
65  |
70  |
75  |
80  |
85  | 
90  |
95  |
100 |
"""
print(axe_6_loc)

axe_6_guess_1 = input(f"{name_1}, Where is the axe?")
if axe_6_guess_1 == (axe_6_num):
    axe_6_cor_1 = True
    
axe_6_guess_2 = input(f"{name_2}, Where is the axe?")
if axe_6_guess_2 == (axe_6_num):
    axe_6_cor_2 = True

axe_7_cor_1 = False
axe_7_cor_2 = False

axe_7_num = "42"

axe_7_loc = """7.
5   |-
10  |
15  |
20  |
25  |
30  |
35  |
40  |
45  |-
50  |
55  |
60  |
65  |
70  |
75  |
80  |
85  | 
90  |
95  |
100 |
"""
print(axe_7_loc)

axe_7_guess_1 = input(f"{name_1}, Where is the axe?")
if axe_7_guess_1 == (axe_7_num):
    axe_7_cor_1 = True

axe_7_guess_2 = input(f"{name_2}, Where is the axe?")
if axe_7_guess_2 == (axe_7_num):
    axe_7_cor_2 = True

axe_8_cor_1 = False
axe_8_cor_2 = False

axe_8_num = "16"

axe_8_loc = """8.
5   |-
10  |
15  |
20  |-
25  |
30  |
35  |
40  |
45  |-
50  |
55  |
60  |
65  |
70  |
75  |
80  |
85  | 
90  |
95  |
100 |
"""
print(axe_8_loc)

axe_8_guess_1 = input(f"{name_1}, Where is the axe?")
if axe_8_guess_1 == (axe_8_num):
    axe_8_cor_1 = True

axe_8_guess_2 = input(f"{name_2}, Where is the axe?")
if axe_8_guess_2 == (axe_8_num):
    axe_8_cor_2 = True

axe_9_cor_1 = False
axe_9_cor_2 = False

axe_9_num = "80"

axe_9_loc = """9.
5   |-
10  |
15  |
20  |
25  |
30  |
35  |
40  |
45  |
50  |
55  |
60  |
65  |
70  |
75  |
80  |-
85  | 
90  |
95  |
100 |
"""
print(axe_9_loc)

axe_9_guess_1 = input(f"{name_1}, Where is the axe?")
if axe_9_guess_1 == (axe_9_num):
    axe_9_cor_1 = True

axe_9_guess_2 = input(f"{name_2}, Where is the axe?")
if axe_9_guess_2 == (axe_9_num):
    axe_9_cor_2 = True

axe_10_cor_1 = False
axe_10_cor_2 = False

axe_10_num = "55"

axe_10_loc = """9.
5   |-
10  |
15  |
20  |
25  |
30  |
35  |
40  |
45  |
50  |
55  |
60  |
65  |
70  |
75  |
80  |-
85  | 
90  |
95  |
100 |
"""
print(axe_10_loc)

axe_10_guess_1 = input(f"{name_1}, Where is the axe?")
if axe_10_guess_1 == (axe_9_num):
    axe_10_cor_1 = True

axe_10_guess_2 = input(f"{name_2}, Where is the axe?")
if axe_10_guess_2 == (axe_9_num):
    axe_10_cor_2 = True

if axe_1_cor_1 == True:
    print(f"1: {name_1} is correct")
if axe_1_cor_1 == False:
    print(f"1: {name_1} is incorrect")
if axe_1_cor_2 == True:
    print(f"1: {name_2} is correct")
if axe_1_cor_2 == False:
    print(f"1: {name_2} is incorrect")

if axe_2_cor_1 == True:
    print(f"2: {name_1} is correct")
if axe_2_cor_1 == False:
    print(f"2: {name_1} is incorrect")
if axe_2_cor_2 == True:
    print(f"2: {name_2} is correct")
if axe_2_cor_2 == False:
    print(f"2: {name_2} is incorrect")

if axe_3_cor_1 == True:
    print(f"3: {name_1} is correct")
if axe_3_cor_1 == False:
    print(f"3: {name_1} is incorrect")
if axe_3_cor_2 == True:
    print(f"3: {name_2} is correct")
if axe_3_cor_2 == False:
    print(f"3: {name_2} is incorrect")  

if axe_4_cor_1 == True:
    print(f"4: {name_1} is correct")
if axe_4_cor_1 == False:
    print(f"4: {name_1} is incorrect")
if axe_4_cor_2 == True:
    print(f"4: {name_2} is correct")
if axe_4_cor_2 == False:
    print(f"4: {name_2} is incorrect")  

if axe_5_cor_1 == True:
    print(f"5: {name_1} is correct")
if axe_5_cor_1 == False:
    print(f"5: {name_1} is incorrect")
if axe_5_cor_2 == True:
    print(f"5: {name_2} is correct")
if axe_5_cor_2 == False:
    print(f"5: {name_2} is incorrect")  

if axe_6_cor_1 == True:
    print(f"6: {name_1} is correct")
if axe_6_cor_1 == False:
    print(f"6: {name_1} is incorrect")
if axe_6_cor_2 == True:
    print(f"6: {name_2} is correct")
if axe_6_cor_2 == False:
    print(f"6: {name_2} is incorrect")  

if axe_7_cor_1 == True:
    print(f"7: {name_1} is correct")
if axe_7_cor_1 == False:
    print(f"7: {name_1} is incorrect")
if axe_7_cor_2 == True:
    print(f"7: {name_2} is correct")
if axe_7_cor_2 == False:
    print(f"7: {name_2} is incorrect")  

if axe_8_cor_1 == True:
    print(f"8: {name_1} is correct")
if axe_8_cor_1 == False:
    print(f"8: {name_1} is incorrect")
if axe_8_cor_2 == True:
    print(f"8: {name_2} is correct")
if axe_8_cor_2 == False:
    print(f"8: {name_2} is incorrect")  

if axe_9_cor_1 == True:
    print(f"9: {name_1} is correct")
if axe_9_cor_1 == False:
    print(f"9: {name_1} is incorrect")
if axe_9_cor_2 == True:
    print(f"9: {name_2} is correct")
if axe_9_cor_2 == False:
    print(f"9: {name_2} is incorrect")  

if axe_10_cor_1 == True:
    print(f"10: {name_1} is correct")
if axe_10_cor_1 == False:
    print(f"10: {name_1} is incorrect")
if axe_10_cor_2 == True:
    print(f"10: {name_2} is correct")
if axe_10_cor_2 == False:
    print(f"10: {name_2} is incorrect")  